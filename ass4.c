/********************
 * Yair Shpitzer
 * 313285942
 * 01
 * ass4
 *******************/


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <math.h>
#include <assert.h>


const int TRUE = 1;
const int FALSE = 0;

// Move logical representation
typedef struct {
    char srcPiece, srcRow, srcCol, destPiece, destRow, destCol, promotionPiece;
    int iSrc, jSrc, iDest, jDest;
    int isWhite, isCapture, isPromotion, isCheck, isMate, isLegal, isSrcRow, isSrcCol, canMove, wasPromoted;
} Move;

typedef  struct {
    int i;
    int j;
} King;

// PGN characters
const char PAWN = 'P';
const char ROOK = 'R';
const char KNIGHT = 'N';
const char BISHOP = 'B';
const char QUEEN = 'Q';
const char KING = 'K';
const char CAPTURE = 'x';
const char PROMOTION = '=';
const char CHECK = '+';
const char MATE = '#';
const char FIRST_COL = 'a';
const char FIRST_ROW = '1';

// FEN & Board characters
const char WHITE_PAWN = 'P';
const char WHITE_ROOK = 'R';
const char WHITE_KNIGHT = 'N';
const char WHITE_BISHOP = 'B';
const char WHITE_QUEEN = 'Q';
const char WHITE_KING = 'K';
const char BLACK_PAWN = 'p';
const char BLACK_ROOK = 'r';
const char BLACK_KNIGHT = 'n';
const char BLACK_BISHOP = 'b';
const char BLACK_QUEEN = 'q';
const char BLACK_KING = 'k';
const int WHITE_PAWN_ROW = SIZE - 2;
const int BLACK_PAWN_ROW = 1;

// FEN separator for strtok()
const char* SEP = "/";

// Board characters
const char EMPTY = ' ';


// Functions to handle the board

/***********************************************************************************
 * Function Name: printColumns
 * Input: none
 * Output: void
 * Function Operation: the function prints the letters of the columns of the board
 **********************************************************************************/
void printColumns(){
    char column = toupper(FIRST_COL);
    printf("* |");
    for (int i = 0; i < SIZE; i++) {
        if (i) {
            printf(" ");
        }
        printf("%c", column);
        column++;
    }
    printf("| *\n");
}

/********************************************************************
 * Function Name: printSpacers
 * Input: none
 * Output: void
 * Function Operation: the function prints the limits of the board
 *******************************************************************/
void printSpacers() {
    printf("* -");
    for (int i = 0; i < SIZE; i++) {
        printf("--");
    }
    printf(" *\n");
}

/*********************************************************************************
 * Function Name: createRow
 * Input: char* row, char* fen, int length
 * Output: void
 * Function Operation: the function updates the row according to the fen string
 ********************************************************************************/
void createRow(char* row, char* fen, int length){
    int fenInd = 0;
    for (int i = 0; i < SIZE; i++){
        // this loop puts 'EMPTY' in the empty spaces in the board
        if (isdigit(fen[fenInd])){
            int spaces = fen[fenInd] - '0';
            for (int j = 0; j < spaces; j++){
                row[i++] = EMPTY;
            }
            i--;
            fenInd++;
        } else {
            row[i] = fen[fenInd++];
        }
    }
}

/***********************************************************************************
 * Function Name: createBoard
 * Input: char board[][SIZE], char fen[]
 * Output: void
 * Function Operation: the function updates the board according to the fen string
 **********************************************************************************/
void createBoard(char board[][SIZE], char fen[]){
    // seperate the fen string into rows
    char* row = strtok(fen, SEP);
    int length = (int) strlen(row);
    int i = 0;
    while (row != NULL){
        createRow(board[i++], row, length);
        row = strtok(NULL, SEP);
    }
}

/*******************************************************************************
 * Function Name: printBoard
 * Input: char board[][SIZE]
 * Output: void
 * Function Operation: the function prints the board according to instructions
 ******************************************************************************/
void printBoard(char board[][SIZE]){
    printColumns();
    printSpacers();
    for (int i = 0; i < SIZE; i++){
        printf("%d |", SIZE - i);
        for (int j = 0; j < SIZE; j++){
            if (board[i][j] == EMPTY){
                printf(" |");
            } else {
                printf("%c|", board[i][j]);
            }
        }
        printf(" %d\n", SIZE - i);
    }
    printSpacers();
    printColumns();
}

/*******************************************************************************
 * Function Name: check borders
 * Input: int i, int j
 * Output: int (0 or 1)
 * Function Operation: the function checks if i and j are in borders of board
 ******************************************************************************/
int checkBorders(int i , int j){
    if (i < 0 || i >= SIZE || j < 0 || j >= SIZE)
        return FALSE;
    return TRUE;
}

/*********************************************************************************
 * Function Name: updateBoard
 * Input: char board[][SIZE], int iSrc, int jSrc, int iDest, int jDest
 * Output: void
 * Function Operation: the function updates the board according to given indexes
 ********************************************************************************/
void updateBoard(char board[][SIZE], int iSrc, int jSrc, int iDest, int jDest){
    char temp = board[iSrc][jSrc];
    board[iSrc][jSrc] = EMPTY;
    board[iDest][jDest] = temp;
}


// Functions for dealing with situation of Check

/**********************************************************************************************************
 * Function Name: findKing
 * Input: char board[][SIZE], Move *move
 * Output: King king
 * Function Operation: the function finds the king of the right color. fields of king are initialized,
 *                     but according to chess rules the king must be on board, and that's why it will
 *                     always find the real location
 *********************************************************************************************************/
King findKing(char board[][SIZE], Move *move){
    King king;

    //  initialize king
    king.i = 0;
    king.j = 0;

    for (int i = 0; i < SIZE; i++){
        for (int j = 0; j < SIZE; j++){
            if (move->isWhite && board[i][j] == WHITE_KING){
                king.i = i;
                king.j = j;
                return king;
            }
            if (!move->isWhite && board[i][j] == BLACK_KING){
                king.i = i;
                king.j = j;
                return king;
            }
        }
    }

    return  king;
}

/**********************************************************************************************************
 * Function Name: threatFromRow
 * Input: char board[][SIZE], Move *move, King king
 * Output: int (0 or 1)
 * Function Operation: the function checks if the king of the right color is threatened from it's row
 *********************************************************************************************************/
int threatFromRow(char board[][SIZE], Move *move, King king){
    // check left
    for (int j = king.j - 1; j >= 0; j--){
        if (move->isWhite && (board[king.i][j] == BLACK_QUEEN || board[king.i][j] == BLACK_ROOK)){
            return TRUE;
        }
        if (!move->isWhite && (board[king.i][j] == WHITE_QUEEN || board[king.i][j] == WHITE_ROOK)){
            return TRUE;
        }
        if (board[king.i][j] != EMPTY){
            break;
        }
    }

    // check right
    for (int j = king.j + 1; j < SIZE; j++){
        if (move->isWhite && (board[king.i][j] == BLACK_QUEEN || board[king.i][j] == BLACK_ROOK)){
            return TRUE;
        }
        if (!move->isWhite && (board[king.i][j] == WHITE_QUEEN || board[king.i][j] == WHITE_ROOK)){
            return TRUE;
        }
        if (board[king.i][j] != EMPTY){
            break;
        }
    }

    return FALSE;
}

/**********************************************************************************************************
 * Function Name: threatFromCol
 * Input: char board[][SIZE], Move *move, King king
 * Output: int (0 or 1)
 * Function Operation: the function checks if the king of the right color is threatened from it's col
 *********************************************************************************************************/
int threatFromCol(char board[][SIZE], Move *move, King king){
    // check up
    for (int i = king.i - 1; i >= 0; i--){
        if (move->isWhite && (board[i][king.j] == BLACK_QUEEN || board[i][king.j] == BLACK_ROOK)){
            return TRUE;
        }
        if (!move->isWhite && (board[i][king.j] == WHITE_QUEEN || board[i][king.j] == WHITE_ROOK)){
            return TRUE;
        }
        if (board[i][king.j] != EMPTY){
            break;
        }
    }

    // check down
    for (int i = king.i + 1; i < SIZE; i++){
        if (move->isWhite && (board[i][king.j] == BLACK_QUEEN || board[i][king.j] == BLACK_ROOK)){
            return TRUE;
        }
        if (!move->isWhite && (board[i][king.j] == WHITE_QUEEN || board[i][king.j] == WHITE_ROOK)){
            return TRUE;
        }
        if (board[i][king.j] != EMPTY){
            break;
        }
    }

    return FALSE;
}

/************************************************************************************************************
 * Function Name: threatFromDiagonals
 * Input: char board[][SIZE], Move *move, King king
 * Output: int (0 or 1)
 * Function Operation: the function checks if the king of the right color is threatened from it's diagonals
 ***********************************************************************************************************/
int threatFromDiagonals(char board[][SIZE], Move *move, King king){
    int iDest = king.i;
    int jDest = king.j;

    // check down-right
    int j = jDest + 1;
    for (int i = iDest + 1; i < SIZE && j < SIZE; i++, j++) {
        if (move->isWhite && (board[i][j] == BLACK_QUEEN || board[i][j] == BLACK_BISHOP)) {
            return TRUE;
        }
        if (!move->isWhite && (board[i][j] == WHITE_QUEEN || board[i][j] == WHITE_BISHOP)) {
            return TRUE;
        }
        if (board[i][j] != EMPTY) {
            break;
        }
    }
    // check up-right
    j = jDest + 1;
    for (int i = iDest - 1; i >= 0 && j < SIZE; i--, j++) {
        if (move->isWhite && (board[i][j] == BLACK_QUEEN || board[i][j] == BLACK_BISHOP)) {
            return TRUE;
        }
        if (!move->isWhite && (board[i][j] == WHITE_QUEEN || board[i][j] == WHITE_BISHOP)) {
            return TRUE;
        }
        if (board[i][j] != EMPTY) {
            break;
        }
    }
    // check up-left
    j = jDest - 1;
    for (int i = iDest - 1; i >= 0 && j >= 0; i--, j--) {
        if (move->isWhite && (board[i][j] == BLACK_QUEEN || board[i][j] == BLACK_BISHOP)) {
            return TRUE;
        }
        if (!move->isWhite && (board[i][j] == WHITE_QUEEN || board[i][j] == WHITE_BISHOP)) {
            return TRUE;
        }
        if (board[i][j] != EMPTY) {
            break;
        }
    }
    // check down-left
    j = jDest - 1;
    for (int i = iDest + 1; i < SIZE && j >= 0; i++, j--) {
        if (move->isWhite && (board[i][j] == BLACK_QUEEN || board[i][j] == BLACK_BISHOP)) {
            return TRUE;
        }
        if (!move->isWhite && (board[i][j] == WHITE_QUEEN || board[i][j] == WHITE_BISHOP)) {
            return TRUE;
        }
        if (board[i][j] != EMPTY) {
            break;
        }
    }

    return FALSE;
}

/**********************************************************************************************************
 * Function Name: threatFromKnight
 * Input: char board[][SIZE], Move *move, King king
 * Output: int (0 or 1)
 * Function Operation: the function checks if the king of the right color is threatened from a knight
 *********************************************************************************************************/
int threatFromKnight(char board[][SIZE], Move *move, King king){
    int i = king.i;
    int j = king.j;

    // first up-left
    if (move->isWhite && checkBorders(i - 2, j - 1) && board[i - 2][j - 1] == BLACK_KNIGHT){
        return TRUE;
    }
    if (!move->isWhite && checkBorders(i - 2, j - 1) && board[i - 2][j - 1] == WHITE_KNIGHT){
        return TRUE;
    }

    // second up-left
    if (move->isWhite && checkBorders(i - 1, j - 2) && board[i - 1][j - 2] == BLACK_KNIGHT){
        return TRUE;
    }
    if (!move->isWhite && checkBorders(i - 1, j - 2) && board[i - 1][j - 2] == WHITE_KNIGHT){
        return TRUE;
    }

    // first down-left
    if (move->isWhite && checkBorders(i + 1, j - 2) && board[i + 1][j - 2] == BLACK_KNIGHT){
        return TRUE;
    }
    if (!move->isWhite && checkBorders(i + 1, j - 2) && board[i + 1][j - 2] == WHITE_KNIGHT){
        return TRUE;
    }

    // second down-left
    if (move->isWhite && checkBorders(i + 2, j - 1) && board[i + 2][j - 1] == BLACK_KNIGHT){
        return TRUE;
    }
    if (!move->isWhite && checkBorders(i + 2, j - 1) && board[i + 2][j - 1] == WHITE_KNIGHT){
        return TRUE;
    }

    // first down-right
    if (move->isWhite && checkBorders(i + 2, j + 1) && board[i + 2][j + 1] == BLACK_KNIGHT){
        return TRUE;
    }
    if (!move->isWhite && checkBorders(i + 2, j + 1) && board[i + 2][j + 1] == WHITE_KNIGHT){
        return TRUE;
    }

    // second down-right
    if (move->isWhite && checkBorders(i + 1, j + 2) && board[i + 1][j + 2] == BLACK_KNIGHT){
        return TRUE;
    }
    if (!move->isWhite && checkBorders(i + 1, j + 2) && board[i + 1][j + 2] == WHITE_KNIGHT){
        return TRUE;
    }

    // first up-right
    if (move->isWhite && checkBorders(i - 2, j + 1) && board[i - 2][j + 1] == BLACK_KNIGHT){
        return TRUE;
    }
    if (!move->isWhite && checkBorders(i - 2, j + 1) && board[i - 2][j + 1] == WHITE_KNIGHT){
        return TRUE;
    }

    // second up-right
    if (move->isWhite && checkBorders(i - 1, j + 2) && board[i - 1][j + 2] == BLACK_KNIGHT){
        return TRUE;
    }
    if (!move->isWhite && checkBorders(i - 1, j + 2) && board[i - 1][j + 2] == WHITE_KNIGHT){
        return TRUE;
    }

    return FALSE;
}

/**********************************************************************************************************
 * Function Name: threatFromPawn
 * Input: char board[][SIZE], Move *move, King king
 * Output: int (0 or 1)
 * Function Operation: the function checks if the king of the right color is threatened from a pawn
 *********************************************************************************************************/
int threatFromPawn(char board[][SIZE], Move *move, King king){
    int i = king.i;
    int j = king.j;

    if (move->isWhite && ((checkBorders(i - 1, j - 1) && board[i - 1][j - 1] == BLACK_PAWN) ||
                        (checkBorders(i - 1, j + 1) && board[i - 1][j + 1] == BLACK_PAWN))){
        return TRUE;
    }

    if (!move->isWhite && ((checkBorders(i + 1, j - 1) && board[i + 1][j - 1] == WHITE_PAWN) ||
                         (checkBorders(i + 1, j + 1) && board[i + 1][j + 1] == WHITE_PAWN))){
        return TRUE;
    }

    return FALSE;
}

/**********************************************************************************
 * Function Name: willExpose
 * Input: char board[][SIZE], Move *move, int i, int j
 * Output: int (0 or 1)
 * Function Operation: the function checks if a certain move will expose the king
 *********************************************************************************/
int willExpose(char board[][SIZE], Move *move, int iSrc, int jSrc){
    // updating the board temporarily
    updateBoard(board, iSrc, jSrc, move->iDest, move->jDest);

    // finding the king's location
    King king = findKing(board, move);

    if (threatFromRow(board, move, king) || threatFromCol(board, move, king) ||
        threatFromDiagonals(board, move, king) || threatFromKnight(board, move, king) ||
        threatFromPawn(board, move, king)){
        // return the board to it's right position
        updateBoard(board, move->iDest, move->jDest, iSrc, jSrc);
        return TRUE;
    }

    // return the board to it's right position
    updateBoard(board, move->iDest, move->jDest, iSrc, jSrc);
    return FALSE;
}

/**********************************************************************************
* Function Name: willCheck
* Input: char board[][SIZE], Move *move, int i, int j
* Output: int (0 or 1)
* Function Operation: the function checks if a certain move will cause Check
*********************************************************************************/
int willCheck(char board[][SIZE], Move *move, int iSrc, int jSrc){
    // updating the board and the color temporarily
    if (!move->wasPromoted) {
        updateBoard(board, iSrc, jSrc, move->iDest, move->jDest);
    }
    move->isWhite = !move->isWhite;

    // finding the king's location
    King king = findKing(board, move);

    if (threatFromRow(board, move, king) || threatFromCol(board, move, king) ||
        threatFromDiagonals(board, move, king) || threatFromKnight(board, move, king) ||
        threatFromPawn(board, move, king)){
        // return the board to it's right position
        if (!move->wasPromoted) {
            updateBoard(board, move->iDest, move->jDest, iSrc, jSrc);
        }
        move->isWhite = !move->isWhite;
        return TRUE;
    }

    // return the board to it's right position
    if (!move->wasPromoted) {
        updateBoard(board, move->iDest, move->jDest, iSrc, jSrc);
    }
    move->isWhite = !move->isWhite;
    return FALSE;
}


// Functions to check if move is possible

/**********************************************************************************************************
 * Function Name: canMovePawn
 * Input: char board[][SIZE], Move *move
 * Output: void
 * Function Operation: the function checks if the way of a rook in the right color is clear to the dest
 *********************************************************************************************************/
int canMovePawn(char board[][SIZE], Move *move, int iSrc, int jSrc){
    int iDest = move->iDest;
    int jDest = move->jDest;

    if (!((move->isWhite && board[iSrc][jSrc] == move->srcPiece) ||
          (!move->isWhite && board[iSrc][jSrc] == tolower(move->srcPiece)))){
        return FALSE;
    }

    if (move->isWhite){
        if (move->isCapture && iSrc == iDest + 1 && abs(jSrc - jDest) == 1 && !willExpose(board, move, iSrc, jSrc)){
            move->iSrc = iSrc;
            move->jSrc = jSrc;
            return TRUE;
        }
        if (!move->isCapture && iSrc == iDest + 1 && jSrc == jDest && !willExpose(board, move, iSrc, jSrc)){
            move->iSrc = iSrc;
            move->jSrc = jSrc;
            return TRUE;
        }
        if (!move->isCapture && iSrc == WHITE_PAWN_ROW && board[iDest + 1][jDest] == EMPTY &&
            iSrc == iDest + 2 && jSrc == jDest && !willExpose(board, move, iSrc, jSrc)){
            move->iSrc = iSrc;
            move->jSrc = jSrc;
            return TRUE;
        }
    } else {
        if (move->isCapture && iSrc == iDest - 1 && abs(jSrc - jDest) == 1 && !willExpose(board, move, iSrc, jSrc)){
            move->iSrc = iSrc;
            move->jSrc = jSrc;
            return TRUE;
        }
        if (!move->isCapture && iSrc == iDest - 1 && jSrc == jDest && !willExpose(board, move, iSrc, jSrc)){
            move->iSrc = iSrc;
            move->jSrc = jSrc;
            return TRUE;
        }
        if (!move->isCapture && iSrc == BLACK_PAWN_ROW && board[iDest - 1][jDest] == EMPTY &&
            iSrc == iDest - 2 && jSrc == jDest && !willExpose(board, move, iSrc, jSrc)){
            move->iSrc = iSrc;
            move->jSrc = jSrc;
            return TRUE;
        }
    }

    return FALSE;
}

/*****************************************************************************************
 * Function Name: canMoveRook
 * Input: char board[][SIZE], Move *move
 * Output: int (0 or 1)
 * Function Operation: the function checks if a rook in the right color can move to dest
 ****************************************************************************************/
int canMoveRook(char board[][SIZE], Move *move, int iSrc, int jSrc){
    int iDest = move->iDest;
    int jDest = move->jDest;

    if (!((move->isWhite && board[iSrc][jSrc] == move->srcPiece) ||
         (!move->isWhite && board[iSrc][jSrc] == tolower(move->srcPiece)))){
        return FALSE;
    }

    if (iSrc != iDest && jSrc != jDest){
        return FALSE;
    }

    // check rows
    if (iSrc == iDest){
        // check left
        if (jSrc > jDest){
            for (int j = jSrc - 1; j > jDest; j--){
                if (board[iSrc][j] != EMPTY){
                    return FALSE;
                }
            }
        } else {
            // check right
            for (int j = jSrc + 1; j < jDest; j++){
                if (board[iSrc][j] != EMPTY){
                    return FALSE;
                }
            }
        }
    // check columns
    } else if (jSrc == jDest){
        // check up
        if (iSrc > iDest){
            for (int i = iSrc - 1; i > iDest; i--){
                if (board[i][jSrc] != EMPTY){
                    return FALSE;
                }
            }
        } else {
            // check down
            for (int i = iSrc + 1; i < iDest; i++){
                if (board[i][jSrc] != EMPTY){
                    return FALSE;
                }
            }
        }
    }

    return TRUE;
}

/*******************************************************************************************
 * Function Name: canMoveKnight
 * Input: char board[][SIZE], Move *move
 * Output: int (0 or 1)
 * Function Operation: the function checks if a knight in the right color can move to dest
 ******************************************************************************************/
int canMoveKnight(char board[][SIZE], Move *move, int iSrc, int jSrc){
    int iDest = move->iDest;
    int jDest = move->jDest;

    if (!((move->isWhite && board[iSrc][jSrc] == move->srcPiece) ||
          (!move->isWhite && board[iSrc][jSrc] == tolower(move->srcPiece)))){
        return FALSE;
    }

    if ((abs(iSrc - iDest) == 1 && abs(jSrc - jDest) == 2) ||
        (abs(iSrc - iDest) == 2 && abs(jSrc - jDest) == 1)){
        return TRUE;
    }

    return FALSE;
}

/*******************************************************************************************
 * Function Name: canMoveBishop
 * Input: char board[][SIZE], Move *move
 * Output: int (0 or 1)
 * Function Operation: the function checks if a bishop in the right color can move to dest
 ******************************************************************************************/
int canMoveBishop(char board[][SIZE], Move *move, int iSrc, int jSrc){
    int iDest = move->iDest;
    int jDest = move->jDest;

    if (!((move->isWhite && board[iSrc][jSrc] == move->srcPiece) ||
          (!move->isWhite && board[iSrc][jSrc] == tolower(move->srcPiece)))){
        return FALSE;
    }

    // if the path between source and dest is not a diagonal
    if (abs(iSrc - iDest) != abs(jSrc - jDest)){
        return FALSE;
    }

    // check down-right
    if (iSrc < iDest && jSrc < jDest){
        int j = jSrc + 1;
        for (int i = iSrc + 1; i < iDest && j < jDest; i++, j++){
            if (board[i][j] != EMPTY){
                return FALSE;
            }
        }
    }

    // check down-left
    if (iSrc < iDest && jDest < jSrc){
        int j = jSrc - 1;
        for (int i = iSrc + 1; i < iDest && j > jDest; i++, j--){
            if (board[i][j] != EMPTY){
                return FALSE;
            }
        }
    }

    // check up-left
    if (iSrc > iDest && jSrc > jDest){
        int j = jSrc - 1;
        for (int i = iSrc - 1; i > iDest && j > jDest; i--, j--){
            if (board[i][j] != EMPTY){
                return FALSE;
            }
        }
    }

    // check up-right
    if (iSrc > iDest && jSrc < jDest){
        int j = jSrc + 1;
        for (int i = iSrc - 1; i > iDest && j > jDest; i--, j++){
            if (board[i][j] != EMPTY){
                return FALSE;
            }
        }
    }

    return TRUE;
}

/*******************************************************************************************
 * Function Name: canMoveQueen
 * Input: char board[][SIZE], Move *move
 * Output: int (0 or 1)
 * Function Operation: the function checks if a queen in the right color can move to dest
 ******************************************************************************************/
int canMoveQueen(char board[][SIZE], Move *move, int iSrc, int jSrc){
    int iDest = move->iDest;
    int jDest = move->jDest;

    if (!((move->isWhite && board[iSrc][jSrc] == move->srcPiece) ||
          (!move->isWhite && board[iSrc][jSrc] == tolower(move->srcPiece)))){
        return FALSE;
    }

    // if the path between source and dest is not a diagonal or a straight line
    if ((abs(iSrc - iDest) != abs(jSrc - jDest)) && iSrc != iDest && jSrc != jDest){
        return FALSE;
    }

    // check rows
    if (iSrc == iDest){
        // check left
        if (jSrc > jDest){
            for (int j = jSrc - 1; j > jDest; j--){
                if (board[iSrc][j] != EMPTY){
                    return FALSE;
                }
            }
        } else {
            // check right
            for (int j = jSrc + 1; j < jDest; j++){
                if (board[iSrc][j] != EMPTY){
                    return FALSE;
                }
            }
        }
    // check columns
    } else if (jSrc == jDest){
        // check up
        if (iSrc > iDest){
            for (int i = iSrc - 1; i > iDest; i--){
                if (board[i][jSrc] != EMPTY){
                    return FALSE;
                }
            }
        } else {
            // check down
            for (int i = iSrc + 1; i < iDest; i++){
                if (board[i][jSrc] != EMPTY){
                    return FALSE;
                }
            }
        }
    }

    // check diagonals

    // check down-right
    if (iSrc < iDest && jSrc < jDest){
        int j = jSrc + 1;
        for (int i = iSrc + 1; i < iDest && j < jDest; i++, j++){
            if (board[i][j] != EMPTY){
                return FALSE;
            }
        }
    }

    // check down-left
    if (iSrc < iDest && jDest < jSrc){
        int j = jSrc - 1;
        for (int i = iSrc + 1; i < iDest && j > jDest; i++, j--){
            if (board[i][j] != EMPTY){
                return FALSE;
            }
        }
    }

    // check up-left
    if (iSrc > iDest && jSrc > jDest){
        int j = jSrc - 1;
        for (int i = iSrc - 1; i > iDest && j > jDest; i--, j--){
            if (board[i][j] != EMPTY){
                return FALSE;
            }
        }
    }

    // check up-right
    if (iSrc > iDest && jSrc < jDest){
        int j = jSrc + 1;
        for (int i = iSrc - 1; i > iDest && j > jDest; i--, j++){
            if (board[i][j] != EMPTY){
                return FALSE;
            }
        }
    }

    return TRUE;
}

/*******************************************************************************************
 * Function Name: canMoveKing
 * Input: char board[][SIZE], Move *move
 * Output: int (0 or 1)
 * Function Operation: the function checks if king in the right color can move to dest
 ******************************************************************************************/
int canMoveKing(char board[][SIZE], Move *move, int iSrc, int jSrc){
    int iDest = move->iDest;
    int jDest = move->jDest;

    if (!((move->isWhite && board[iSrc][jSrc] == move->srcPiece) ||
          (!move->isWhite && board[iSrc][jSrc] == tolower(move->srcPiece)))){
        return FALSE;
    }

    if (abs(iSrc - iDest) <= 1 && abs(jSrc - jDest) <= 1){
        return TRUE;
    }

    return FALSE;
}

/*******************************************************************************************
 * Function Name: canMove
 * Input: char board[][SIZE], Move *move
 * Output: int (0 or 1)
 * Function Operation: the function checks if the piece given by PNG can move to dest
 ******************************************************************************************/
int canMove(char board[][SIZE], Move *move) {
    // when both source's row and column were given in png
    if (move->isSrcCol && move->isSrcRow) {
        if (move->srcPiece == PAWN) {
            move->canMove = canMovePawn(board, move, move->iSrc, move->jSrc);
        }
        if (move->srcPiece == KNIGHT) {
            move->canMove = canMoveKnight(board, move, move->iSrc, move->jSrc);
        }
        if (move->srcPiece == ROOK) {
            move->canMove = canMoveRook(board, move, move->iSrc, move->jSrc);
        }
        if (move->srcPiece == BISHOP) {
            move->canMove = canMoveBishop(board, move, move->iSrc, move->jSrc);
        }
        if (move->srcPiece == QUEEN) {
            move->canMove = canMoveQueen(board, move, move->iSrc, move->jSrc);
        }
        if (move->srcPiece == KING) {
            move->canMove = canMoveKing(board, move, move->iSrc, move->jSrc);
        }


        if (move->canMove && !willExpose(board, move, move->iSrc, move->jSrc)) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    // when source's row was given in png
    if (move->isSrcRow && !move->isSrcCol) {
        for (int j = 0; j < SIZE; j++) {
            if ((move->isWhite && board[move->iSrc][j] == move->srcPiece) ||
                (!move->isWhite && board[move->iSrc][j] == tolower(move->srcPiece))) {
                if (move->srcPiece == PAWN) {
                    move->canMove = canMovePawn(board, move, move->iSrc, j);
                }
                if (move->srcPiece == KNIGHT) {
                    move->canMove = canMoveKnight(board, move, move->iSrc, j);
                }
                if (move->srcPiece == ROOK) {
                    move->canMove = canMoveRook(board, move, move->iSrc, j);
                }
                if (move->srcPiece == BISHOP) {
                    move->canMove = canMoveBishop(board, move, move->iSrc, j);
                }
                if (move->srcPiece == QUEEN) {
                    move->canMove = canMoveQueen(board, move, move->iSrc, j);
                }
                if (move->srcPiece == KING) {
                    move->canMove = canMoveKing(board, move, move->iSrc, j);
                }
            }
            if (move->canMove == TRUE && !willExpose(board, move, move->iSrc, j)) {
                move->jSrc = j;
                return TRUE;
            }
        }
    }

    // when source's col was given in png
    if (move->isSrcCol && !move->isSrcRow) {
        for (int i = 0; i < SIZE; i++) {
            if (((move->isWhite && board[i][move->jSrc] == move->srcPiece) ||
                 (!move->isWhite && board[i][move->jSrc] == tolower(move->srcPiece)))) {
                if (move->srcPiece == PAWN) {
                    move->canMove = canMovePawn(board, move, i, move->jSrc);
                }
                if (move->srcPiece == KNIGHT) {
                    move->canMove = canMoveKnight(board, move, i, move->jSrc);
                }
                if (move->srcPiece == ROOK) {
                    move->canMove = canMoveRook(board, move, i, move->jSrc);
                }
                if (move->srcPiece == BISHOP) {
                    move->canMove = canMoveBishop(board, move, i, move->jSrc);
                }
                if (move->srcPiece == QUEEN) {
                    move->canMove = canMoveQueen(board, move, i, move->jSrc);
                }
                if (move->srcPiece == KING) {
                    move->canMove = canMoveKing(board, move, i, move->jSrc);
                }
            }
            if (move->canMove == TRUE && !willExpose(board, move, i, move->jSrc)) {
                move->iSrc = i;
                return TRUE;
            }
        }
    }

    // when source's row and column weren't given in png
    if (!move->isSrcCol && !move->isSrcRow) {
        for (int i = 0; i < SIZE; i++) {
            for (int j = 0; j < SIZE; j++) {
                if (((move->isWhite && board[i][j] == move->srcPiece) ||
                     (!move->isWhite && board[i][j] == tolower(move->srcPiece)))) {
                    if (move->srcPiece == PAWN) {
                        move->canMove = canMovePawn(board, move, i, j);
                    }
                    if (move->srcPiece == KNIGHT) {
                        move->canMove = canMoveKnight(board, move, i, j);
                    }
                    if (move->srcPiece == ROOK) {
                        move->canMove = canMoveRook(board, move, i, j);
                    }
                    if (move->srcPiece == BISHOP) {
                        move->canMove = canMoveBishop(board, move, i, j);
                    }
                    if (move->srcPiece == QUEEN) {
                        move->canMove = canMoveQueen(board, move, i, j);
                    }
                    if (move->srcPiece == KING) {
                        move->canMove = canMoveKing(board, move, i, j);
                    }
                }
                if (move->canMove == TRUE && !willExpose(board, move, i, j)) {
                    move->iSrc = i;
                    move->jSrc = j;
                    return TRUE;
                }
            }
        }
    }

    return FALSE;
}

/*****************************************************************
 * Function Name: isLegal
 * Input: char board[][SIZE], Move *move
 * Output: int (0 or 1)
 * Function Operation: the function checks if the move is legal
 ****************************************************************/
int isLegal(char board[][SIZE], Move *move){
    int i = move->iDest;
    int j = move->jDest;

    // check capture conditions
    if (move->isWhite && move->isCapture && (board[i][j] == EMPTY || isupper(board[i][j]))){
        return FALSE;
    }
    if (!move->isWhite && move->isCapture && (board[i][j] == EMPTY || islower(board[i][j]))){
        return FALSE;
    }
    if (!move->isCapture && board[i][j] != EMPTY){
        return FALSE;
    }

    // check promotion conditions
    if (move->isPromotion) {
        if (move->srcPiece != PAWN){
            return FALSE;
        }
        if (move->isWhite && move->iDest != 0){
            return FALSE;
        }
        if (!move->isWhite && move->iDest != SIZE - 1){
            return FALSE;
        }
    } else if (move->srcPiece == PAWN){
        if (move->isWhite && move->iDest == 0){
            return FALSE;
        }
        if (!move->isWhite && move->iDest == SIZE - 1) {
            return FALSE;
        }
    }

    // check if piece can move
    if (!canMove(board, move)){
        return FALSE;
    }

    if (move->isPromotion){
        updateBoard(board, move->iSrc, move->jSrc, move->iDest, move->jDest);
        move->wasPromoted = TRUE;
        if (move->isWhite) {
            board[move->iDest][move->jDest] = move->promotionPiece;
        } else {
            board[move->iDest][move->jDest] = tolower(move->promotionPiece);
        }
    }

    // check Check and Mate conditions
    if ((move->isCheck || move->isMate) && !(willCheck(board, move, move->iSrc, move->jSrc))){
        return FALSE;
    }
    if (!(move->isCheck || move->isMate) && willCheck(board, move, move->iSrc, move->jSrc)){
        return FALSE;
    }

    return TRUE;
}




/**********************************************************************************************
 * Function Name: parseMove
 * Input: char board[][SIZE], char pgn[], int isWhiteTurn
 * Output: Move
 * Function Operation: the function gets png string and updates move's fields according to it
 *********************************************************************************************/
Move parseMove(char board[][SIZE], char pgn[], int isWhiteTurn){
    Move move;
    int length = (int) strlen(pgn);

    // check if white's turn
    if (isWhiteTurn){
        move.isWhite = TRUE;
    } else {
        move.isWhite = FALSE;
    }

    // search for special characters
    if (memchr(pgn, CAPTURE, length) != NULL){
        move.isCapture = TRUE;
    } else {
        move.isCapture = FALSE;
    }
    if (memchr(pgn, PROMOTION, length) != NULL){
        move.isPromotion = TRUE;
    } else {
        move.isPromotion = FALSE;
    }
    if (memchr(pgn, CHECK, length) != NULL){
        move.isCheck = TRUE;
    } else {
        move.isCheck = FALSE;
    }
    if (memchr(pgn, MATE, length) != NULL){
        move.isMate = TRUE;
    } else {
        move.isMate = FALSE;
    }

    // assume that source's column and row are not given
    move.isSrcRow = FALSE;
    move.isSrcCol = FALSE;

    // saves dest's row and column, and also source's if exist
    int count = 0;
    for (int i = length; i >= 0; i--){
        if (i == 0){
            if (isupper(pgn[i]))
                move.srcPiece = pgn[i];
            else
                move.srcPiece = PAWN;
        }

        if (count < 2) {
            if (islower(pgn[i]) && pgn[i] != CAPTURE) {
                move.destCol = pgn[i];
                count++;
            } else {
                if (isdigit(pgn[i])) {
                    move.destRow = pgn[i];
                    count++;
                }
            }
        } else {
            if (islower(pgn[i]) &&  pgn[i] != CAPTURE){
                move.srcCol = pgn[i];
                move.isSrcCol = TRUE;
            } else {
                if (isdigit(pgn[i])){
                    move.srcRow = pgn[i];
                    move.isSrcRow = TRUE;
                }
            }
        }

        if (pgn[i] == PROMOTION){
            move.promotionPiece = pgn[i + 1];
        }
    }

    // translating the row and column chars to ints
    move.iDest = SIZE - (move.destRow - FIRST_ROW) - 1;
    move.jDest = move.destCol - FIRST_COL;
    if (move.isSrcRow){
        move.iSrc = SIZE - (move.srcRow - FIRST_ROW) - 1;
    }
    if (move.isSrcCol){
        move.jSrc = move.srcCol - FIRST_COL;
    }

    // find destPiece
    move.destPiece = board[move.iDest][move.jDest];

    move.wasPromoted = FALSE;

    return move;
}

/*****************************************************************************
 * Function Name: makeMove
 * Input: char board[][SIZE], char pgn[], int isWhiteTurn
 * Output: int (0 or 1)
 * Function Operation: the function gets a png and checks if move is legal
 ****************************************************************************/
int makeMove(char board[][SIZE], char pgn[], int isWhiteTurn){
    Move move = parseMove(board, pgn, isWhiteTurn);

    if (isLegal(board, &move)){
        if (!move.wasPromoted) {
            updateBoard(board, move.iSrc, move.jSrc, move.iDest, move.jDest);
            return TRUE;
        }
        return TRUE;
    }

    return FALSE;
}

